﻿using ComputingService.Contexts;
using ComputingService.Interfaces;
using ComputingService.Middleware;
using ComputingService.Models;
using ComputingService.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ComputingService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            ConfigureContexts(services);
            ConfigureRepositories(services);
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler();
            }

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseMvc();
        }

        public void ConfigureContexts(IServiceCollection services)
        {
            string memDbName = "Computing";

            services.AddDbContext<CalculationContext>(options => options.UseInMemoryDatabase(memDbName));
        }

        public void ConfigureRepositories(IServiceCollection services)
        {
            services.AddSingleton<IRepository<Calculation>, CalculationRepository>();
        }
    }
}
