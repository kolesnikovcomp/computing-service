﻿using ComputingService.Contexts;
using ComputingService.Loggers;
using ComputingService.Models;
using ComputingService.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ComputingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComputingController : ControllerBase
    {
        private readonly CalculationContext _calculationContext;
        private readonly ILogger _logger;

        public ComputingController(CalculationContext calculationContext, ILogger<ComputingController> logger)
        {
            _calculationContext = calculationContext;
            _logger = logger;
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting item: {ID}", id);
            Calculation calc = _calculationContext.Calculations.Find(id);
            if (calc == null)
            {
                _logger.LogInformation(LoggingEvents.GetItemNotFound, "Item with ID: {ID} not found");
                return NotFound();
            }

            CalculationViewModel result = new CalculationViewModel {Id = calc.Id, Result = calc.Result};
            return new ObjectResult(result);
        }

        [HttpPost("calculate")]
        public IActionResult Calculate([FromBody]Calculation calc)
        {
            if (calc?.OperationType == null)
            {
                return BadRequest();
            }

            _logger.LogInformation(LoggingEvents.GenerateItems, "Getting operation: {Operation}", calc.OperationType);
            calc.Operation = calc.Operation.LoadByType(calc.OperationType);

            _logger.LogInformation(LoggingEvents.GenerateItems, "Calculating item: {Operation}, {Numbers}",
                calc.OperationType, calc.Numbers);
            calc.Result = calc.Operation?.Operate(calc.Numbers);

            _calculationContext.Calculations.Add(calc);
            _logger.LogInformation(LoggingEvents.InsertItem, "Inserting item");
            _calculationContext.SaveChanges();

            return Get(calc.Id);
        }
    }
}
