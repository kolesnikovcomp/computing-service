﻿using System.Collections.Generic;
using ComputingService.Contexts;
using ComputingService.Interfaces;
using ComputingService.Models;
using Microsoft.EntityFrameworkCore;

namespace ComputingService.Repositories
{
    public class CalculationRepository : IRepository<Calculation>
    {
        private readonly CalculationContext _db;

        public CalculationRepository()
        {
            _db = new CalculationContext();
        }

        public IEnumerable<Calculation> GetAll()
        {
            return _db.Calculations;
        }

        public Calculation Get(int id)
        {
            return _db.Calculations.Find(id);
        }

        public void Create(Calculation item)
        {
            _db.Calculations.Add(item);
        }

        public void Update(Calculation item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Calculation calc = _db.Calculations.Find(id);
            if (calc != null)
                _db.Calculations.Remove(calc);
        }

        public void Save(Calculation item)
        {
            _db.SaveChanges();
        }
    }
}
