﻿using System;
using System.Collections.Generic;
using System.Linq;
using ComputingService.Models.Enums;

namespace ComputingService.Models.Operations
{
    public class DivisionOperation : Operation
    {
        public DivisionOperation()
        {
            Type = OperationType.Division;
        }

        public override double Operate(IEnumerable<double> numbers)
        {
            double total = 0;
            bool isFirst = false;

            if (numbers.Any(num => num == 0))
            {
                throw new ApplicationException("Division by zero");
            }

            foreach (double num in numbers)
            {
                total = !isFirst ? num : total / num;
                isFirst = true;
            }

            return total;
        }
    }
}
