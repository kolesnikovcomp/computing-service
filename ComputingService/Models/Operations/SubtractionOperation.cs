﻿using System.Collections.Generic;
using ComputingService.Models.Enums;

namespace ComputingService.Models.Operations
{
    public class SubtractionOperation : Operation
    {
        public SubtractionOperation()
        {
            Type = OperationType.Subtraction;
        }

        public override double Operate(IEnumerable<double> numbers)
        {
            double total = 0;
            bool isFirst = false;

            foreach (double num in numbers)
            {
                total = !isFirst ? num : total - num;
                isFirst = true;
            }

            return total;
        }
    }
}
