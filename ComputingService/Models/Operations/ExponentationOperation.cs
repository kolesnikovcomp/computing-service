﻿using System;
using System.Collections.Generic;
using System.Linq;
using ComputingService.Models.Enums;

namespace ComputingService.Models.Operations
{
    public class ExponentationOperation : Operation
    {
        public ExponentationOperation()
        {
            Type = OperationType.Exponentiation;
        }

        public override double Operate(IEnumerable<double> numbers)
        {
            double total;

            if (numbers.Count() > 1)
            {
                throw new ApplicationException("Must have only one number for exponentation");
            }

            total = Math.Exp(numbers.First());

            return total;
        }
    }
}
