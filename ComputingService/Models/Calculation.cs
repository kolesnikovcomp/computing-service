﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using ComputingService.Models.Enums;
using Newtonsoft.Json;

namespace ComputingService.Models
{
    [Table("Calculations")]
    public class Calculation
    {
        [Key, DisplayName("ID")]
        public int Id { get; set; }
        [NotMapped]
        [JsonIgnore]
        public Operation Operation { get; set; }
        [Required]
        [EnumDataType(typeof(OperationType))]
        public OperationType OperationType { get; set; }
        [JsonIgnore]
        public string InternalNumbersData { get; set; }
        public double? Result { get; set; }
        [NotMapped]
        [Required]
        [MinLength(1)]
        public IEnumerable<double> Numbers
        {
            get
            {
                return !string.IsNullOrEmpty(InternalNumbersData)
                    ? Array.ConvertAll(InternalNumbersData?.Split(';'), double.Parse)
                    : null;
            }
            set
            {
                InternalNumbersData = string.Join(";", value.Select(p => p.ToString(CultureInfo.CurrentCulture)).ToArray());
            }
        }

    }
}
