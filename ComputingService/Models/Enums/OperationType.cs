﻿namespace ComputingService.Models.Enums
{
    public enum OperationType
    {
        Addition,
        Subtraction,
        Multiplication,
        Division,
        Exponentiation
    }
}
