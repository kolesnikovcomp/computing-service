﻿using System.Collections.Generic;
using ComputingService.Models.Enums;
using ComputingService.Models.Operations;

namespace ComputingService.Models
{
    public abstract class Operation
    {
        public OperationType Type { get; set; }
        public abstract double Operate(IEnumerable<double> numbers);
    }

    public static class OperationExtensionMethods
    {
        public static Operation LoadByType(this Operation operation, OperationType type)
        {
            switch (type)
            {
                case OperationType.Addition:
                    return new AdditionOperation();
                case OperationType.Subtraction:
                    return new SubtractionOperation();
                case OperationType.Multiplication:
                    return new MultiplicationOperation();
                case OperationType.Division:
                    return new DivisionOperation();
                case OperationType.Exponentiation:
                    return new ExponentationOperation();
            }

            return null;
        }
    }
}
