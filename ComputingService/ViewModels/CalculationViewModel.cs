﻿namespace ComputingService.ViewModels
{
    public class CalculationViewModel
    {
        public int Id { get; set; }
        public double? Result { get; set; }
    }
}
