﻿using Newtonsoft.Json;

namespace ComputingService.ViewModels
{
    public class ExceptionViewModel
    {
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
}
