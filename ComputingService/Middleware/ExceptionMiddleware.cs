﻿using System;
using System.Net;
using System.Threading.Tasks;
using ComputingService.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ComputingService.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/plain";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                if (ex is ApplicationException)
                {
                    var exceptionResult = new ExceptionViewModel {Message = ex.Message};
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(exceptionResult, Formatting.Indented));
                }
            }
        }
    }
}
