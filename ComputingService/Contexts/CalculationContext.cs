﻿using ComputingService.Models;
using Microsoft.EntityFrameworkCore;

namespace ComputingService.Contexts
{
    public class CalculationContext : DbContext
    {
        public CalculationContext()
        { }

        public CalculationContext(DbContextOptions<CalculationContext> options)
            : base(options)
        { }

        public DbSet<Calculation> Calculations { get; set; }
    }
}
